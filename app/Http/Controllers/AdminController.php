<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\EmailPending;
use App\Models\User as User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminController extends Controller {


//    protected $loginPath = '/admin/login';

    public function __construct()
    {
//        $this->middleware('auth.admin');
    }

    public function login()
    {
        return View('admin.login');
    }

    public function viewMenu()
    {
        return View('admin.thucdon');
    }

    public function viewAdmin()
    {
        $data=[
            'admins'=>Admin::orderBy('emp_id','asc')->get()
        ];

        return View('admin.admin')->with($data);
    }

    public function viewUser()
    {
//        $users = array();
        $data=[
            'users'=>User::orderBy('emp_id','asc')->get()
        ];

//        return View('admin.nguoidung')->with('users',User::all());
        return View('admin.nguoidung')->with($data);
    }

    public function newUser()
    {

    }

    public function viewEmail()
    {
        $data=[
            'emails'=>EmailPending::orderBy('created_at','desc')->get()
        ];

        return View('admin.emailpending')->with($data);
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        return View('admin.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createUser()
	{
		//
        return View('admin.createUser');
	}

    public function deleteUser()
    {
        return View('admin.deleteUser');
    }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
