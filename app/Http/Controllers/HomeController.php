<?php
/**
 * Created by PhpStorm.
 * User: ndang
 * Date: 5/22/2015
 * Time: 5:01 PM
 */
namespace App\Http\Controllers;

class HomeController extends  Controller
{
    public function index()
    {
        return View('frontend.home');
    }

}