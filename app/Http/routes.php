<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

//Route::group(['middleware' => 'auth.admin'], function()
//{
//    Route::get('/admin/login', 'AdminController@login'
//    );
//
//});

Route::get('/admin', 'AdminController@index');
Route::get('/admin/login','AdminController@login');
Route::get('/admin/nguoi-dung','AdminController@viewUser');
Route::get('/admin/thuc-don','AdminController@viewMenu');
Route::get('/admin/emails-pending','AdminController@viewEmail');
Route::get('/admin/quan-tri-vien','AdminController@viewAdmin');
