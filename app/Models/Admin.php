<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {

	//
    protected $table = "admins";

    protected $hidden = ['id','password', 'remember_token'];

    protected $fillable =['name', 'email', 'password', 'avatar'];

}
