<?php
/**
 * Created by PhpStorm.
 * User: ndang
 * Date: 5/21/2015
 * Time: 1:53 PM
 */
use Illuminate\Database\Seeder;
use App\Models\Admin as Admin;

class AdminSeeder extends Seeder
{
    public function run()
    {
        Admin::truncate();

        Admin::create([
           'name' => 'joe',
            'emp_id' => '12345abc',
            'email' => 'joe@gmail.com',
            'password' => Hash::make('12345'),
        ]);
    }
}