<?php
/**
 * Created by PhpStorm.
 * User: ndang
 * Date: 5/21/2015
 * Time: 1:53 PM
 */
use Illuminate\Database\Seeder;
use App\Models\EmailPending as Emails;

class EmailPendingSeeder extends Seeder
{
    public function run()
    {
        Emails::truncate();


        Emails::create([
            'email'=>'theeagle.276@gmail.com'
        ]);
    }
}