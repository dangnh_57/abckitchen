<?php
/**
 * Created by PhpStorm.
 * User: ndang
 * Date: 5/21/2015
 * Time: 1:53 PM
 */
use Illuminate\Database\Seeder;
use App\Models\User as User;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::truncate();

        User::create([
            'name'=>'jin',
            'emp_id'=>'34wddf',
            'email'=>'sd@rgrg.com',
            'password'=>Hash::make('jqka')
        ]);
        User::create([
            'name'=>'kim',
            'emp_id'=>'3efe432ff',
            'email'=>'kim@rgrg.com',
            'password'=>Hash::make('123')
        ]);
        User::create([
            'name'=>'bob',
            'emp_id'=>'3432ff',
            'email'=>'bob@rgrg.com',
            'password'=>Hash::make('jqka')
        ]);
        User::create([
            'name'=>'johny',
            'emp_id'=>'343dede2ff',
            'email'=>'jnd@rgrg.com',
            'password'=>Hash::make('111')
        ]);
        User::create([
            'name'=>'john',
            'emp_id'=>'3432ff',
            'email'=>'sddsds@rgrg.com',
            'password'=>Hash::make('jqka')
        ]);

        User::create([
            'name'=>'zed',
            'emp_id'=>'w2343',
            'email'=>'sqqqd@rgrg.com',
            'password'=>Hash::make('jqka')
        ]);

        User::create([
            'name'=>'guye',
            'emp_id'=>'der45',
            'email'=>'srerd@rgrg.com',
            'password'=>Hash::make('jhg')
        ]);

        User::create([
            'name'=>'john',
            'emp_id'=>'3432ff',
            'email'=>'rtrt@rgrg.com',
            'password'=>Hash::make('546')
        ]);

        User::create([
            'name'=>'kimbal',
            'emp_id'=>'defg55',
            'email'=>'sdffe@rgrg.com',
            'password'=>Hash::make('2fe')
        ]);

        User::create([
            'name'=>'rechard',
            'emp_id'=>'34dd2ff',
            'email'=>'hhhd@rgrg.com',
            'password'=>Hash::make('768')
        ]);
    }
}