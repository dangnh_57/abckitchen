<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Metronic | Data Tables - Managed Datatables</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/select2/select2.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="../assets/global/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css" />
    <link id="style_color" href="../assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" />
    <link href="../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>

<body class="page-header-fixed page-quick-sidebar-over-content ">
@include('admin.layouts.header')
<div class="clear-fix"></div>
<div class="page-container">
@include('admin.layouts.sidebar')
 <div class="page-content-wrapper">
            <div class="page-content">

                <!-- BEGIN PAGE HEADER-->
                <h3 class="page-title">
            Email đang chờ
            </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="index.html">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Email đang chờ</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <!-- <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                        Actions <i class="fa fa-angle-down"></i>
                        </button> -->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" />
                                    <!-- <span class="help-block">
                                            Select date </span> -->
                                    <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar
"></i></button>
                                                </span>
                                </div>
                            </div>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>
                                    <a href="#">Action</a>
                                </li>
                                <li>
                                    <a href="#">Another action</a>
                                </li>
                                <li>
                                    <a href="#">Something else here</a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="#">Separated link</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-user"></i>Table
                            </div>
                            <div class="actions">
                                {{--<a href="#" class="btn btn-default btn-sm" data-toggle="modal">--}}
                                    {{--<i class="fa fa-pencil"></i> Add </a>--}}
                                {{--<a href="#" class="btn btn-default btn-sm">--}}
                                    {{--<i class="fa fa-file-pdf-o"></i> Export Pdf </a>--}}
                                {{--<a href="#" class="btn btn-default btn-sm">--}}
                                    {{--<i class="fa fa-file-excel-o"></i> Export Excel </a>--}}
                                {{--<div class="btn-group">--}}
                                    {{--<a class="btn btn-default btn-sm" href="#" data-toggle="dropdown">--}}
                                        {{--<i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>--}}
                                    {{--</a>--}}
                                    {{--<ul class="dropdown-menu pull-right">--}}
                                        {{--<li>--}}
                                            {{--<a href="#">--}}
                                                {{--<i class="fa fa-pencil"></i> Edit </a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">--}}
                                                {{--<i class="fa fa-trash-o"></i> Delete </a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">--}}
                                                {{--<i class="fa fa-ban"></i> Ban </a>--}}
                                        {{--</li>--}}
                                        {{--<li class="divider">--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href="#">--}}
                                                {{--<i class="i"></i> Make admin </a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="table-checkbox">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        </th>
                                        <th>
                                            STT
                                        </th>
                                        <th>
                                            Mail
                                        </th>
                                        <th>
                                            Tùy chọn
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($emails as $email)
                                    <tr class="odd gradeX">
                                        <td>
                                            <input type="checkbox" class="checkboxes" value="1" />
                                        </td>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            <a href="">
                                    {{$email->email}}</a>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-icon-only red">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            <a href="#" class="btn btn-icon-only green">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add new User</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="" role="form" class="form-horizontal">
                                            <div class="form-body">
                                                <div class="form-group">

                                                    <div class="col-md-9">
                                                        <div class="input-inline input-medium">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                                <input type="email" class="form-control" placeholder="Email Address">
                                                            </div>
                                                        </div>
                                                        <span class="help-inline">
                                            </span>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">

                                                    <div class="col-md-9">
                                                        <div class="input-inline input-medium">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                    </span>
                                                                <input type="text" class="form-control" placeholder="Ma Nhan Vien">
                                                            </div>
                                                        </div>
                                                        <span class="help-inline">
                                             </span>
                                                    </div>
                                                </div>
                                                <!-- <input type="button" class="btn red" value="Save"> -->
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn blue">Save changes</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        </div>
       @include('admin.layouts.footer')
       <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
           <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
           <script src="../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
           <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
           <!-- END CORE PLUGINS -->
           <!-- BEGIN PAGE LEVEL PLUGINS -->
           <script type="text/javascript" src="../assets/global/plugins/select2/select2.min.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/clockface/js/clockface.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
           <script type="text/javascript" src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
           <!-- END PAGE LEVEL PLUGINS -->
           <!-- BEGIN PAGE LEVEL SCRIPTS -->
           <script src="../assets/global/scripts/metronic.js" type="text/javascript"></script>
           <script src="../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
           <script src="../assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
           <script src="../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
           <script src="../assets/admin/pages/scripts/table-managed.js"></script>
           <script>
           jQuery(document).ready(function() {
               Metronic.init(); // init metronic core components
               Layout.init(); // init current layout
               QuickSidebar.init(); // init quick sidebar
               Demo.init(); // init demo features
               TableManaged.init();
               // ComponentsPickers.init();
           });
           </script>
       </body>
       <!-- END BODY -->

       </html>