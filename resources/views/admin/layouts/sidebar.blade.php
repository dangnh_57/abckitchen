<div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
					<form class="sidebar-search " action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active open">
					<a href="javascript:;">
					<i class="fa fa-users"></i>
					<span class="title">Người dùng</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<li class="active">
							<a href="/admin/nguoi-dung">

							Người lao động</a>
						</li>

						<li>
							<a href="/admin/quan-tri-vien">

							Admin</a>
						</li>

					</ul>
				</li>
				<li>
					<a href="/admin/thuc-don">
					<i class="fa fa-cutlery"></i>
					<span class="title">Thực đơn</span>

					</a>

				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-bar-chart"></i>
					<span class="title">Thống kê</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="nguoi_an.html">
							<i class="fa fa-user"></i>
							Người đến ăn</a>
						</li>
						<li>
							<a href="dang_ky_an.html">
							<i class="fa fa-pencil-square-o"></i>
							Đăng ký ăn</a>
						</li>
						<li>
							<a href="chitieu.html">
							<i class="fa fa-money"></i>
							Chi tiêu</a>
						</li>

					</ul>
				</li>

				<li>
					<a href="/admin/emails-pending">
					<i class="fa fa-list-ul"></i>
					<span class="title">Email pending</span>

					</a>

				</li>

			</ul>

</div>
</div>