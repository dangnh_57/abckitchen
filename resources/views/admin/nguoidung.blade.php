<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Metronic | Data Tables - Managed Datatables</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/select2/select2.css" />
    <link rel="stylesheet" type="text/css" href="../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="../assets/global/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css" />
    <link id="style_color" href="../assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" />
    <link href="../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

<body class="page-header-fixed page-quick-sidebar-over-content ">
@include('admin.layouts.header')
<div class="clear-fix"></div>
<div class="page-container">
@include('admin.layouts.sidebar')
     <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

                    <!-- /.modal -->
                    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                Managed Datatables <small>managed datatable samples</small>
                </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="index.html">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Người dùng</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">NLD</a>
                            </li>
                        </ul>

                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <div class="row">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box yellow portlet-body">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i>Table
                                </div>
                                <div class="actions">
                                    <!-- <a class="btn default" data-toggle="modal" href="#basic">
                                        View Demo </a> -->
                                    <a href="" class="btn btn-default btn-sm" data-toggle="modal">
                                        <i class="fa fa-pencil"></i> Add </a>
                                    <a href="#" class="btn btn-default btn-sm">
                                        <i class="fa fa-file-pdf-o"></i> Export Pdf </a>
                                    <a href="#" class="btn btn-default btn-sm">
                                        <i class="fa fa-file-excel-o"></i> Export Excel </a>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" href="#" data-toggle="dropdown">
                                            <i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-pencil"></i> Edit </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-trash-o"></i> Delete </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-ban"></i> Ban </a>
                                            </li>
                                            <li class="divider">
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="i"></i> Make admin </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Add new User</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="" role="form" class="form-horizontal">
                                                <div class="form-body">
                                                    <div class="form-group">

                                                        <div class="col-md-9">
                                                            <div class="input-inline input-medium">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                                    <input type="email" class="form-control" placeholder="Email Address">
                                                                </div>
                                                            </div>
                                                            <span class="help-inline">
                                                </span>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">

                                                        <div class="col-md-9">
                                                            <div class="input-inline input-medium">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                        </span>
                                                                    <input type="text" class="form-control" placeholder="Ma Nhan Vien">
                                                                </div>
                                                            </div>
                                                            <span class="help-inline">
                                                 </span>
                                                        </div>
                                                    </div>
                                                    <!-- <input type="button" class="btn red" value="Save"> -->
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn blue">Save changes</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            </div>

                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th class="table-checkbox">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                            </th>
                                            <th>STT</th>
                                            <th>
                                                Ma Nhan Vien
                                            </th>
                                            <th>
                                                Email
                                            </th>
                                            <th>
                                                Status
                                            </th>

                                        </tr>
                                    </thead>
                                    <?php $i=0 ?>
                                    <tbody>
                                    @foreach($users as $user)
                                    <?php $i++ ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <input type="checkbox" class="checkboxes" value="1" />
                                            </td>
                                            <td>{{$i}}</td>
                                            <td>
                                                {{$user->emp_id}}
                                            </td>
                                            <td>
                                                <a href="">
                                        {{$user->email}} </a>
                                            </td>
                                            <td>
                                                @if($user->is_active)
                                                     <span class="label label-sm label-success">
                                                                         Đã Active </span>
                                                @else
                                                     <span class="label label-sm label-default">
                                                      bị khóa </span>
                                                 @endif
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
</div>
@include('admin.layouts.footer')
<script type="text/javascript" src="../assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
    <script src="../assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
    <script src="../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
    <script src="../assets/admin/pages/scripts/table-managed.js"></script>
    <script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        // QuickSidebar.init(); // init quick sidebar
        // Demo.init(); // init demo features
        TableManaged.init();
    });
    </script>
</body>
<!-- END BODY -->

</html>
