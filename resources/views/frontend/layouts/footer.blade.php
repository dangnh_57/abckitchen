<footer class="js--page-footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-sm-4">
                        <div class="footer-widgets__social">
                            <img class="push-down-10" alt="Footer Logo" src="images/logo.png" width="35" height="35">
                            <p class="push-down-15">Adipiscing elit. Ut ullamcorper consectetur, non lacinia turpis suscipit non. Estibulum nu nc lacus, tincidunt non odio eu, scelerisque tristique quam.</p>
                            <a class="social-container" href=""><span class="zocial-facebook"></span></a>
                            <a class="social-container" href=""><span class="zocial-twitter"></span></a>
                            <a class="social-container" href="#"><span class="zocial-googleplus"></span></a>
                            <a class="social-container" href=""><span class="zocial-youtube"></span></a>
                        </div>
                    </div>
                    <div class="col-xs-12  col-sm-4">
                        <nav class="footer-widgets__navigation">
                            <div class="footer-wdgets__heading--line">
                                <h4 class="footer-widgets__heading">Navigation</h4>
                            </div>
                            <ul class="nav nav-footer">
                                <li><a href="index.html">Trang chủ</a></li>
                                <li><a href="shop.html">Món ăn</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-xs-12  col-sm-4">
                        <div class="footer-widgets__navigation">
                            <div class="footer-wdgets__heading--line">
                                <h4 class="footer-widgets__heading">Contact Us</h4>
                            </div>
                            <a class="footer__link" href="#">ABC Kitchen.</a>
                            <br> Cầu Giấy,
                            <br> Hà Nội, Việt Nam
                            <br>
                            <a class="footer__link--small" href="contact-2.html">Xem Bản đồ<span class="glyphicon glyphicon-chevron-right glyphicon--footer-small"></span></a>
                            <br>
                            <br>
                            <a class="footer__link" href="#"><span class="glyphicon glyphicon-earphone glyphicon--footer"></span> +386 31 567 537</a>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  col-sm-6">
                        <div class="footer__text--link">
                            Based on<a class="footer__link" href="#"> Organique</a> HTML Theme © Copyright 2014. Images of products by <a class="footer__link" href="http://www.nutrisslim.com/" target="_blank">Nutrisslim</a>.
                        </div>
                    </div>
                    <div class="col-xs-12  col-sm-6">
                        <div class="footer__text">
                            Made with <span class="glyphicon  glyphicon-heart"></span> by <a class="footer__link" href="http://www.proteusthemes.com/" target="_blank">ProteusThemes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript">
    function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "js/app.min.js";
        document.body.appendChild(element);
    }
    if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
    </script>
    <script src="js/plugin/jquery.min.js"></script>
    <script src="js/plugin/TweenMax.min.js"></script>
    <script src="js/plugin/ScrollToPlugin.min.js"></script>
    <script src="js/smothscroll.js"></script>
    <script src="js/plugin/wow.min.js"></script>
    <script>
    new WOW().init();
    </script>
    <script src="jquery.lazyload.js" type="text/javascript"></script>
    </body>
    </html>