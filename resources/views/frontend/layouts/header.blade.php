    <div class="top  js--fixed-header-offset">
        <div class="container">
            <div class="row">
                <div class="col-xs-12  col-sm-6">
                    <div class="top__slogan">
                        Made with <small><span class="glyphicon  glyphicon-heart  tertiary-color"></span></small> for a healthy lifestyle
                    </div>
                </div>
                <div class="col-xs-12  col-sm-6">
                    <div class="top__menu">
                        <ul class="nav  nav-pills">
                            <li><a href="#registerModal" role="button" data-toggle="modal">Đăng ký</a></li>
                            <li><a href="#loginModal" role="button" data-toggle="modal">Đăng nhập</a></li>
                            <li><a href="#">Tài khoản</a></li>
                            <li class="dropdown  js--mobile-dropdown">
                                |
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal register-->
    <div class="modal  fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content  center">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3><span class="light">Register</span> to ABC</h3>
                    <hr class="divider">
                </div>
                <div class="modal-body">
                    <form action="#" class="push-down-15">
                        <div class="form-group">
                            <input type="text" id="name" class="form-control  form-control--contact" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="text" id="email" class="form-control  form-control--contact" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="text" id="subject" class="form-control  form-control--contact" placeholder="Password">
                        </div>
                        <button type="submit" class="btn  btn-primary">Đăng ký</button>
                    </form>
                    <a data-toggle="modal" role="button" href="#loginModal" data-dismiss="modal">Đã có tài khoản?</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal login-->
    <div class="modal  fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content  center">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3><span class="light">Login</span> to ABC</h3>
                    <hr class="divider">
                </div>
                <div class="modal-body">
                    <form action="#" class="push-down-15">
                        <div class="form-group">
                            <input type="text" id="name" class="form-control  form-control--contact" placeholder="Username">
                            <!-- <i class="fa fa-user"></i> -->
                        </div>
                        <div class="form-group">
                            <input type="text" id="subject" class="form-control  form-control--contact" placeholder="Password">
                            <!-- <i class="fa fa-lock"></i> -->
                        </div>
                        <button type="submit" class="btn  btn-primary">Đăng nhập</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <header class="header js--navbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-10  col-md-3">
                    <div class="header-logo">
                        <a href="index.html"><img alt="Logo" src="images/logo.png" width="90" height="90"></a>
                    </div>
                </div>
                <div class="col-xs-2  visible-sm  visible-xs">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle  collapsed" data-toggle="collapse" data-target="#collapsible-navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12  col-md-7">
                    <nav class="navbar  navbar-default" role="navigation">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse  navbar-collapse" id="collapsible-navbar">
                            <ul class="nav  navbar-nav">
                                <li class="">
                                    <a href="index.html" class="">Trang chủ</a>
                                </li>
                                <li class="">
                                    <a href="shop.html" class="">Món ăn</a>
                                </li>
                                <li class="">
                                    <a href="contact.html" class="">Liên hệ</a>
                                </li>
                            </ul>

                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                </div>
                <div class="col-xs-12  col-md-2  hidden-sm  hidden-xs">

                </div>
            </div>
        </div>
        <!--Search open pannel-->
    </header>