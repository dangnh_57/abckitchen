<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from www.proteusthemes.com/themes/organique-html/home-business.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Apr 2015 03:20:54 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ProteusNet">
    <link rel="icon" type="image/ico" href="images/favicon.png">
    <title>ABCKitchen</title>
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/style.min.css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="css/animate.min.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- Google fonts -->
    <script type="text/javascript">
    WebFontConfig = {
        google: {
            families: ['Arvo:700:latin', 'Open+Sans:400,600,700:latin']
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
    </script>
</head>

<body>
@include('frontend.layouts.header')
@yield('content')
@include('frontend.layouts.footer')